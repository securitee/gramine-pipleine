FROM ubuntu:22.04

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install --no-install-recommends -y build-essential \
	autoconf bison gawk ninja-build python3 python3-click python3-jinja2 \
	wget git meson python3-toml libcurl4-openssl-dev libprotobuf-c-dev \
	protobuf-c-compiler python3-pip python3-protobuf linux-headers-generic \
	pkg-config \
	&& rm -rf /var/lib/apt/lists/*
